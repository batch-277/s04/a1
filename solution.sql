-- Artists with letter D
SELECT * FROM artists WHERE name LIKE "%d%";

-- songs with length less than 230
SELECT * FROM songs WHERE length < 230;

-- join albums and songs table
SELECT albums.album_title, songs.song_name, songs.length FROM albums INNER JOIN songs ON albums.id = songs.album_id;

-- joinse artists and albums tables with letter a in its name
SELECT * FROM albums INNER JOIN artists ON albums.artist_id = artists.id WHERE album_title LIKE "%a%";

--- sort albums from Z-A
SELECT * FROM albums ORDER BY album_title DESC LIMIT 0, 4;

--- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * FROM albums INNER JOIN songs ON albums.id = songs.album_id ORDER BY album_title;